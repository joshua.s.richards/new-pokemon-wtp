import React, { useContext } from "react";
import { regions } from "../common/constants";
import { SelectedRegionsContext } from "../common/contexts";
import { SilhouetteContext } from "../common/contexts";
import "./Sidebar.scss";

const Sidebar: React.FC = () => {
  const [selectedRegions, selectRegion] = useContext(SelectedRegionsContext);
  const [showSilhouette, setShowSilhouette] = useContext(SilhouetteContext);

  const _renderFilterItem = (text: string): JSX.Element => {
    return (
      <tr key={text}>
        <td>
          <input
            type="checkbox"
            checked={selectedRegions.includes(text)}
            onChange={() => selectRegion(text)}
          />
        </td>
        <td>{text}</td>
      </tr>
    );
  };

  return (
    <div className="Sidebar">
      <h1>Filters</h1>
      <h3>Region</h3>
      <table className="Sidebar__filters">
        <tbody>{regions.map(region => _renderFilterItem(region))}</tbody>
      </table>
      <h3>Silhouettes</h3>
      <input
        type="checkbox"
        checked={showSilhouette}
        onChange={setShowSilhouette}
      />
    </div>
  );
};

export default Sidebar;
