import React from 'react';

import "./Header.scss"

const Header: React.FC = () => {
  return (
    <div className="Header">
      <h1 className="Header__text">Who's That Pokemon?</h1>
    </div>
  )
};

export default Header;
