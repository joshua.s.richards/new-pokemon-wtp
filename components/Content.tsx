import React, { useEffect, useState, useContext } from "react";
import PokemonImage from "./PokemonImage";
import { useGetPokemonEndpoint } from "../hooks/useGetPokemonEndpoint";
import { SelectedRegionsContext } from "../common/contexts";
import { SilhouetteContext } from "../common/contexts";
import "./Content.scss";
import PokemonLoadingIndicator from "./PokemonLoadingIndicator";

const Content: React.FC = () => {
  const [selectedRegions] = useContext(SelectedRegionsContext);
  const [showSilhouette] = useContext(SilhouetteContext);
  const [
    pokemon,
    pokemonIsLoading,
    pokemonHasError,
    getPokemon
  ] = useGetPokemonEndpoint("");
  const [correctAnswerNo, setCorrectAnswerNo] = React.useState(0);
  const [numberOfAnswers, setNumberOfAnswers] = React.useState(0);
  const [answer, setAnswer] = React.useState("");
  const [isAnswered, setIsAnswered] = React.useState(false);
  const [localShowSilhouette, setLocalShowSilhouette] = useState(false);

  useEffect(() => {
    getPokemon(selectedRegions);
  }, []);

  function _onEnter(e: any) {
    if (e.key === "Enter") {
      isAnswered ? _getNextPokemon() : _isAnswerCorrect();
    }
  }

  function _isAnswerCorrect(): void {
    if (answer.toLowerCase() === pokemon.toLowerCase()) {
      setCorrectAnswerNo(correctAnswerNo + 1);
      showSilhouette && setLocalShowSilhouette(true);
    }

    setIsAnswered(true);
    setNumberOfAnswers(numberOfAnswers + 1);
  }

  function _getNextPokemon(): void {
    if (!isAnswered) {
      setNumberOfAnswers(numberOfAnswers + 1);
    }

    setLocalShowSilhouette(false);
    getPokemon(selectedRegions);
    setIsAnswered(false);
    setAnswer("");
  }

  const _renderImage = () => {
    if (!pokemon || pokemonIsLoading) {
      return <PokemonLoadingIndicator />;
    }

    if (pokemonHasError) {
      return (
        <div>
          <span>
            Something went wrong when displaying pokemon. Please try again
          </span>
          <button onClick={() => getPokemon(selectedRegions)}>
            Reload Pokemon
          </button>
        </div>
      );
    }

    return (
      <PokemonImage
        pokemon={`${pokemon}${
          showSilhouette && !localShowSilhouette ? "_new" : ""
        }`}
      />
    );
  };

  return (
    <div className="Content">
      {_renderImage()}
      <div>
        <input
          id="Content__input"
          onChange={e => setAnswer(isAnswered ? "" : e.target.value)}
          onKeyDown={e => _onEnter(e)}
          value={answer}
        />
        <button
          onClick={
            isAnswered ? () => _getNextPokemon() : () => _isAnswerCorrect()
          }
        >
          {isAnswered ? "Next" : "Submit"}
        </button>
        <button onClick={() => _getNextPokemon()}>Skip</button>
      </div>
      {numberOfAnswers > 0 && (
        <span>
          You have successfully guessed {correctAnswerNo} out of{" "}
          {numberOfAnswers}!
        </span>
      )}
      {isAnswered && answer.toLowerCase() !== pokemon.toLowerCase() && (
        <span>Sorry, the correct answer is {pokemon}.</span>
      )}
    </div>
  );
};

export default Content;
