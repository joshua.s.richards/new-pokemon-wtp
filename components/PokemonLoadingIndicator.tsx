import React from "react";
import "./PokemonLoadingIndicator.scss";

const PokemonLoadingIndicator: React.FC = () => {
    return <div className="PokemonLoadingIndicator"/>;
};

export default PokemonLoadingIndicator;
