import React from "react";

interface Props {
  pokemon: string;
}

const PokemonImage: React.FC<Props> = ({ pokemon }) => {
  return <img src={`/static/images/pokemon/${pokemon}.png`} />;
};

export default PokemonImage;
