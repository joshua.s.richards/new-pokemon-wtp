import {createContext} from "react";

export const SelectedRegionsContext = createContext([[''], (region: string) => {}]);

export const SilhouetteContext = createContext([false, () => {}]);
