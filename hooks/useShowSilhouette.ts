import {useState} from "react";

export const useShowSilhouette = (): [boolean, () => void] => {
   const [showSilhouette, setShowSilhouette] = useState(true);
   
   const toggleShowSilhouette = () => {
       setShowSilhouette(!showSilhouette);
   }

   return [showSilhouette, toggleShowSilhouette];
}
