import {useState} from "react";
import axios from 'axios';

export type GetPokemonEndpointResponse = [string, boolean, boolean, (selectedRegions: string[]) => void];

export const useGetPokemonEndpoint = (initialPokemon: string): GetPokemonEndpointResponse => {
    const [pokemon, setPokemon] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [hasError, setHasError] = useState(false);

    const getPokemon = (selectedRegions: string[]): void => {
        setIsLoading(true);
        axios.post('/pokemon', {selectedRegions}).then(response => {
            setPokemon(response.data);
            setHasError(false);
        }).catch(() => {
            setHasError(true);
        }).finally(() => {
            setIsLoading(false);
        });
    }

    return [pokemon, isLoading, hasError, getPokemon];
}
