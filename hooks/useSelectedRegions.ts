import { useState, useMemo, createContext, Context } from "react";
import { regions } from "../common/constants";

export const useSelectedRegions = (): [string[], (region: string) => void] => {
  const [selectedRegions, setSelectedRegions] = useState(regions);

  const toggleSelectedRegion = (region: string) => {
    if (selectedRegions.includes(region)) {
      setSelectedRegions(
        selectedRegions.filter(selectedRegion => selectedRegion !== region)
      );
    } else {
      setSelectedRegions([...selectedRegions, region]);
    }
  };

  return [selectedRegions, toggleSelectedRegion];
};
