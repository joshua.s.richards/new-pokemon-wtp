# Pokemon Website using nextjs

## Project setup
```
npm install
```
### Database setup
```
cd resources
docker build -t pokemondb .
docker run -d -p 3306:3306 --name pokemondb -e MYSQL_ROOT_PASSWORD=password pokemondb
```

### Run website
```
npm run dev
```
