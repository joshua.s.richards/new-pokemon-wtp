const express = require("express");

const router = express.Router();

let mysql = require("mysql");
let connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "password",
  database: "wtp"
});
connection.connect(function(err) {
  if (err) throw err;
  router.post("/pokemon", (req, res) => {
    let insertString = req.body.selectedRegions.reduce(
      (string, value) => string + `region='${value}' OR `,
      "SELECT name FROM pokemon WHERE ("
    );
    insertString =
      insertString.substring(0, insertString.length - 4) +
      ") ORDER BY RAND() LIMIT 1;";
    connection.query(insertString, function(err, result) {
      if (err) throw err;
      res.send(result[0].name.replace(/([\n])/, ""));
    });
  });
});

module.exports = router;
