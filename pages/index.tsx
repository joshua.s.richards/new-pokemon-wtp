import React, { createContext, useMemo } from "react";
import axios from "axios";
import Header from "../components/Header";
import Sidebar from "../components/Sidebar";
import Content from "../components/Content";
import { regions } from "../common/constants";
import { useSelectedRegions } from "../hooks/useSelectedRegions";
import { useShowSilhouette } from "../hooks/useShowSilhouette";
import { SelectedRegionsContext } from "../common/contexts";
import { SilhouetteContext } from "../common/contexts";
import "./App.scss";

const App: React.FC = () => {
  const selectedRegionsHook = useSelectedRegions();
  const silhouetteHook = useShowSilhouette();

  return (
    <SelectedRegionsContext.Provider
      value={useMemo(() => selectedRegionsHook, [selectedRegionsHook[0]])}
    >
      <SilhouetteContext.Provider
        value={useMemo(() => silhouetteHook, [silhouetteHook[0]])}
      >
        <div className="App">
          <Header />
          <Sidebar />
          <Content />
        </div>
      </SilhouetteContext.Provider>
    </SelectedRegionsContext.Provider>
  );
};

export default App;
