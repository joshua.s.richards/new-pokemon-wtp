CREATE TABLE pokemon (
  name varchar(256),
  pokedex_number int,
  region varchar(256)
);

ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';
