import java.awt.Color;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.image.WritableRaster;
import java.util.Random;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.*;
import java.lang.reflect.Array;
import java.util.Arrays;
public class Image
{
	public static void save(String name, BufferedImage image) throws Exception
	{
		File saveAs = new File(name + "_new.png");
		ImageIO.write(image, "png", saveAs);
	}

	public static void darken(String name)
	{
		try
		{
		BufferedImage image = ImageIO.read(new File(name + ".png"));
		int width = image.getWidth();
		int height = image.getHeight();
		WritableRaster raster = image.getRaster();
		for (int col = 0; col < width; col++)
		{
			for (int row = 0; row < height; row++)
			{
				int[] pixels = raster.getPixel(col, row, (int[]) null);
				pixels[0] = 0;
				pixels[1] = 0;
				pixels[2] = 0;
				raster.setPixel(col, row, pixels);
			}
		}
		Image.save(name,image);
		}
		catch(Exception e)
		{
			System.out.println(name);
		}
	}
}
