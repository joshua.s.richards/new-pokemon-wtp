def main():
    file = open("insertIntoPokemonTable.sql","w+")
    source_file = open("Pokemon.txt","r")
    pokemon_list = source_file.readlines()

    file.write("INSERT INTO pokemon (name,pokedex_number,region) \n VALUES ");

    pokedex_number=1
    region=''

    for i in pokemon_list:
        if pokedex_number<=151:
            region='Kanto'
        elif pokedex_number<=251:
            region='Johto'
        elif pokedex_number<=386:
            region='Hoenn'
        elif pokedex_number<=493:
            region='Sinnoh'
        elif pokedex_number<=649:
            region='Unova'
        elif pokedex_number<=721:
            region='Kalos'
        else:
            region='Alola'                    
        file.write('("'+i+'","'+str(pokedex_number)+'","'+region+'"),\n')
        pokedex_number+=1

    file.write(";")

    file.close()

if __name__=="__main__":
    main()
